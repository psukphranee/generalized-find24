
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  5 17:20:22 2018

@author: broncop
"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import api_test
'''
POSTGRES = {
    'user': 'broncop',
    'pw': 'broncop',
    'db': 'mydb2',
    'host': 'localhost',
    'port': '5432',
}

app = Flask(__name__)

app.config['TESTING'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s/%(db)s' % POSTGRES

db = SQLAlchemy(app)
'''
db = SQLAlchemy()

class outputs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    input_set = db.Column(db.String(80), primary_key=False)
    target = db.Column(db.String(10), unique=False, nullable=False)
    #set_ = db.Column(db.String(120), unique=False, nullable=False)
    output = db.Column(db.String(120), unique=False, nullable=False)

class input_history(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    input_set = db.Column(db.String(80), primary_key=False)
    target_ih = db.Column(db.String(80), unique=False, nullable=False)
    results = db.Column(db.String(20), unique=False, nullable=True)
    
    