#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 15:25:46 2018

@author: broncop
"""

def make_complement(pair, original_set):
    
    #there is possibility that this removes repeats, which we don't want
    complement_set = [x for x in original_set if x not in pair]
    
    return complement_set