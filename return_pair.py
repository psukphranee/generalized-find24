#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 13:31:25 2018

@author: broncop
"""

from Pair import Pair
import copy

def return_pair(arg_1, arg_2):
    
    #case both integers
    if(type(arg_1) == int and type(arg_2) == int):
        m = Pair(arg_1,arg_2)
        return list([m])
    
    #cases only one is an integer
    if(type(arg_1) == int):
        temp_arg = []
        temp_arg = copy.deepcopy(arg_2)
        temp_arg.process(arg_1)
        return list([temp_arg])
    if(type(arg_2) == int):
        temp_arg = []
        temp_arg = copy.deepcopy(arg_1)
        temp_arg.process(arg_2)
        return list([temp_arg])
    
    #case both Pair objects
    list_pairs = [None] * 6
    for i in range(6):
        list_pairs[i] = copy.deepcopy(arg_1)
        
    for i in range(6):
        list_pairs[i] = copy.deepcopy(arg_1)
        list_pairs[i].process(arg_2.value[i], arg_2.val_str[i])
    
    return list_pairs