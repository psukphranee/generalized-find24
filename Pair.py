#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 22:30:23 2018

@author: broncop
"""

class Pair:
    
    sum_ = 0
    product = 0
    diff_1 = 0
    diff_2 = 0
    quot_1 = 0
    quot_2 = 0
    
    value = None
    val_str = None
    
    def process(self, arg_proc, string=""):
        
        self.sum_ = self.sum_ + arg_proc
        self.product = self.product * arg_proc
        self.diff_1 = self.diff_1 - arg_proc
        self.diff_2 = arg_proc - self.diff_2
        self.quot_1 = (self.quot_1 / arg_proc) if (arg_proc !=0) else 10000000000
        self.quot_2 = (arg_proc / self.quot_2) if (self.quot_2 !=0) else 10000000000
        
        self.value[0] = self.sum_
        self.value[1] = self.product
        self.value[2] = self.diff_1
        self.value[3] = self.diff_2
        self.value[4] = self.quot_1
        self.value[5] = self.quot_2
        
        if string=="":
            self.val_str[0] = "(" + self.val_str[0] + "+" + str(arg_proc) + ")"
            self.val_str[1] = "(" + self.val_str[1] + "*" + str(arg_proc) + ")"
            self.val_str[2] = "(" + self.val_str[2] + "-" + str(arg_proc) + ")"
            self.val_str[3] = "(" + str(arg_proc) + "-" + self.val_str[3] + ")"
            self.val_str[4] = "(" + self.val_str[4] + "/" + str(arg_proc) + ")"
            self.val_str[5] = "(" + str(arg_proc) + "/" + self.val_str[5] + ")"
        else:
            self.val_str[0] = "(" + self.val_str[0] + "+" + string + ")"
            self.val_str[1] = "(" + self.val_str[1] + "*" + string + ")"
            self.val_str[2] = "(" + self.val_str[2] + "-" + string + ")"
            self.val_str[3] = "(" + string + "-" + self.val_str[3] + ")"
            self.val_str[4] = "(" + self.val_str[4] + "/" + string + ")"
            self.val_str[5] = "(" + string + "/" + self.val_str[5] + ")"
    
    def __init__(self, int_1, int_2):
        
        self.value = list([self.sum_, self.product, self.diff_1, self.diff_2, self.quot_1, self.quot_2])
        self.val_str = list(['','','','','',''])
        
        self.sum_ = int_1 + int_2
        self.product = int_1 * int_2
        self.diff_1 = int_1 - int_2
        self.diff_2 = int_2 - int_1
        self.quot_1 = (int_1 / int_2) if (int_2 !=0) else 10000000000
        self.quot_2 = (int_2 / int_1) if (int_1 !=0) else 10000000000
        
        self.val_str[0] = "(" + str(int_1) + "+" + str(int_2) + ")"
        self.val_str[1] = "(" + str(int_1) + "*" + str(int_2) + ")"
        self.val_str[2] = "(" + str(int_1) + "-" + str(int_2) + ")"
        self.val_str[3] = "(" + str(int_2) + "-" + str(int_1) + ")"
        self.val_str[4] = "(" + str(int_1) + "/" + str(int_2) + ")"
        self.val_str[5] = "(" + str(int_2) + "/" + str(int_1) + ")"
        
        self.value[0] = self.sum_
        self.value[1] = self.product
        self.value[2] = self.diff_1
        self.value[3] = self.diff_2
        self.value[4] = self.quot_1
        self.value[5] = self.quot_2
        
        
    def __repr__(self):
        
        out = ""
        
        for i in range(6):
            out = out + ("%s = %s\n" % (str(self.val_str[i]), str(self.value[i])))
            
        return out