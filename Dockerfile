FROM python:3.6-alpine
WORKDIR /app
COPY . /app
RUN apk update && \
 apk add postgresql-libs && \
 apk add --virtual .build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps
#RUN pip install -r requirements.txt
ENV FLASK_APP=api_test
EXPOSE "5000"
CMD ["flask","run", "--host=0.0.0.0"]
