#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 15:52:29 2018

@author: broncop
"""

from api_test import *

temp_list = [1,4,5,6,12]
target = 42

with app.app_context():
    num_of_results = scratch_scripts.outputs.query.count()
    

    count_history = scratch_scripts.input_history.query.filter_by(target_ih=str(target)).count()
        
    out = {"Number of Entries" : str(num_of_results)}
    input_record = scratch_scripts.input_history(input_set="input_set_str", target_ih="targetx", results=5)
    #scratch_scripts.db.session.add(input_record)
    #scratch_scripts.db.session.commit()