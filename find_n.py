#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 15:31:55 2018

@author: broncop
"""
import Pair
from return_pair import *
from make_complement import *
from itertools import combinations
import api_test

from scratch_scripts import *


def find_n(set_, target):
    
    if (len(set_) == 2):
        leaf = return_pair(set_[0],set_[1])
        for i in range(len(leaf)):
            for j in range(6):
                if leaf[i].value[j] == target:
                    temp_str = ("{} = {},").format(leaf[i].val_str[j],leaf[i].value[j])
                    db_output = outputs(input_set=api_test.input_set, target=str(target), output=temp_str) 
                    #db_output = outputs(input_set=input_set, target=str(target), output=temp_str)
                    db.session.add(db_output)
                    db.session.commit()
                    print(temp_str)
                    

    #make list of possible 2 combinations            
    combos = list(combinations(set_,2))
    #loop through the combos
    for i in range(len(combos)):
        current_Pair_list = return_pair(combos[i][0], combos[i][1])
        remove_pair = list([combos[i][0], combos[i][1]])
        
        #debug
        #print(f'paired for removal :\n ({combos[i][0]},{combos[i][1]})\n')
        #print(f'APPEND THIS, current_Pair_list, combo[{i}]:\n{current_Pair_list}\n')
        #print(f'remove_pair:\n{remove_pair}\n')
        
        ## There is a possiblilty that make_complement may remove repeats 
        for j in range(len(current_Pair_list)):
            current_Pair = current_Pair_list[j]
            complement_set = []
            #print(f'making complement of :\n{remove_pair}\n')
            complement_set = make_complement(remove_pair, set_)
            #print(f'complement set is :\n{complement_set}\n')
            complement_set.append(current_Pair)
            #print(f'After removing and appending:\n{complement_set}\n')
            find_n(complement_set, target)