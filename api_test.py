#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  5 15:21:18 2018

@author: broncop
"""

from flask import Flask, request, jsonify, json
from flask_restful import Resource, Api, reqparse
import logging
import find_n

import classes
#from classes import *
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
api = Api(app)

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)

POSTGRES = {
    'user': 'broncop',
    'pw': 'broncop',
    'db': 'mydb2',
    #'host': 'localhost',
    'host': 'db',
    'port': '5432',
}

app.config['TESTING'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s/%(db)s' % POSTGRES

#db = SQLAlchemy(app)

with app.app_context():
    classes.db.init_app(app)
    classes.db.create_all() 
    classes.ma.init_app(app)

logger = logging.getLogger(__name__)
input_set = "xx"

class Input_Data(Resource):
    
    def post(self):
        
        input_data = request.get_json()
        set_ = input_data['set']
        target = input_data['target']
        
        #Split input list and convert entries into integers if possible
        temp_list = set_.split(",")
        for i in range(len(temp_list)):
            try:
                temp_list[i] = int(temp_list[i])
            except ValueError:
                continue
                logger.error(f'Removed item {i} : {temp_list[i]}')
                
        #make a list out of the input set 
        temp_list = [x for x in temp_list if type(x) == int]
        global input_set 
        input_set = str(temp_list)
        
        #see if this input has previously been input
        with app.app_context():
            count_history = classes.input_history.query.filter_by(input_set=str(temp_list), target_ih=str(target)).count()
        
        if count_history != 0:
            out = {"Number of Entries" : str(classes.outputs.query.filter_by(input_set=input_set, target=target).count())}
            return json.dumps(out)
        else:
            find_n.find_n(temp_list, int(target))
            
            with app.app_context():
                num_of_results = classes.outputs.query.filter_by(input_set=input_set, target=target).count()
        
            out = {"Number of Entries" : str(num_of_results)}
            input_record = classes.input_history(input_set=input_set, target_ih=target, results=str(num_of_results))
            classes.db.session.add(input_record)
            classes.db.session.commit()
            return json.dumps(out)        
        
    def get(self): #return jsons of filtered results
        
        #Parse input
        input_data = request.get_json()
        set_ = input_data['set']
        target = input_data['target']
        
        #Split input list and convert entries into integers if possible
        temp_list = set_.split(",")
        for i in range(len(temp_list)):
            try:
                temp_list[i] = int(temp_list[i])
            except ValueError:
                continue
                logger.error(f'Removed item {i} : {temp_list[i]}')
                
        #make a list out of the input set 
        temp_list = [x for x in temp_list if type(x) == int]
        global input_set 
        input_set = str(temp_list)
        
        with app.app_context():
            count_history = classes.input_history.query.filter_by(input_set=str(temp_list), target_ih=str(target)).count()
        
        if count_history == 0:
            return "This set need to be processed first. Submit as POST, then you may submit form as GET"
        else:
            query_buffer = classes.outputs.query.filter_by(input_set=str(temp_list), target=str(target))
            outputs_buffer = classes.OutputSchema(many=True)
            outputs_output = outputs_buffer.dump(query_buffer)
            return jsonify({'test': outputs_output})      
            

    #query the input_history table and return jsons of input history

class GetHistory(Resource): 
    
    def get(self):
         query_buffer = classes.input_history.query.all()
         input_history_schema_buffer = classes.InputHistorySchema(many=True)
         input_history_output = input_history_schema_buffer.dump(query_buffer)
         return jsonify({'test': input_history_output})
     
api.add_resource(Input_Data, '/')
api.add_resource(GetHistory, '/gethistory')
